# OSINT-NG 

```
Tool with several modules for searching public data.

OSINT-NG [version 1.0.a]  - C0D3DG0D - ALAN RAMALHO:

________    _________.___ __________________          _______    ________ 
\_____  \  /   _____/   |\      \__    ___/          \      \  /  _____/ 
 /   |   \ \_____  \|   |/   |   \|    |     ______  /   |   \/   \  ___ 
/    |    \/        \   /    |    \    |    /_____/ /    |    \    \_\  \
\_______  /_______  /___\____|__  /____|            \____|__  /\______  /
        \/        \/            \/                          \/        \/ 



Use: osint-ng --help

Usage: osint-ng [OPTIONS]

Options:
  -m, --mode TEXT    Select the osint attack mode.
                     [cpf][cep][cnpj]   -Find CPF,CEP and CNPJ information.
                     [sms][tts]    -Send SMS or TTS message for a number.
                     [audio][call] -Call with AUDIO or CALL back.
                     [nbr-scan]    -Phone verify information.
                     [ip-scan]     -IPV6 verify information.
  -t, --target TEXT  Define the target information.
                     [cep]  -Size: xxxxxxx 8 format number.
                     [call] -Size: DDD+number 11 format number, BINA & TARGET.
  -f, --filter TEXT  Filter options to extract. [OPTIONAL]
                     IN
                     DEVELOPMENT.
  -q, --quiet TEXT   Show informations without banner. [OPTIONAL]
                     -q True or
                     --quiet True.
  -v, --version      Show the current version.
  --help             Show this message and exit.
```
